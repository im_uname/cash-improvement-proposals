# CHIPs: Processes for consensus changes

Proposed by: @imaginary_username

Discussion URL: https://bitcoincashresearch.org/t/chips-a-more-detailed-process-recommendation/309

## Summary

We define a Cash Improvement Proposal (CHIP) as the public documentation of a shared ecosystem process to achieve one positive change on the BCH network. It shall include descriptions of the proposal itself, elaboration of its risk/reward and any relevant technical aspects, and outline necessary agreement-building processes for its own acceptance.

Any change that involves consensus rules is subject to the evaluation schedule outlined in section "Scheduled upgrade" below.

A CHIP shall include the following sections at a minimum: Summary, Motivation and Benefits, Technical Description, Specification, Implementation Costs and Risks, Ongoing Costs and Risks, Current Implementations, Test Cases, Evaluation of Alternatives, Security Considerations, List of Major Stakeholders, and Statements. An example of these sections can be found below, along with general expectations for future CHIPs.

For all subsequent CHIPs, an important part of gauging support and informing adoption is gaining support from members of the "List of Major Stakeholders" section. This document itself is an exception; as the document establishing the process itself, its support is gauged by successes and/or rejections of subsequent proposals following its process.

## Background, assumptions and principles

This section is unique to this document, and lays out some of the background assumptions underlying the process. Future CHIPs may lay them out in a similar section, but are not required to do so.

1. Protocol changes have inherent costs in network and social coordination. No matter how well-meaning or apparently uncontroversial, they always bring with them risks of technical, economic and community destabilization. Such risks are generally more acute in consensus changes and less so in network policies.

2. Despite the inherent cost, as of 2021, the Bitcoin Cash protocol still has room for practical improvements. There exist possible protocol changes that, if executed well, can bring about greater net value to the network and currency compared to a route of protocol ossification.

3. By the nature of open source software development and public blockchains, no part of this document or subsequent CHIPs can coerce any actor into anything against their will. Node development teams are always free to write what they want, while miners, businesses and service operators are always free to use software they see fit.

4. While miners and economic actors ultimately have the power to decide between different consensus rules, decisionmaking via competing forks is highly destructive for coin value and network effect. It is desirable to have a less confrontational process that seeks to inform, persuade, and yield clear outcomes for evaluation efforts.

5. A basic requirement for any change proposal is careful and good-faith research that takes into account its risks and benefits. Proposals that do not make good-faith attempts to take observable risks into account should be met with skepticism.

6. Any power that CHIPs have can only be persuasive in nature. We expect the more technical considerations, economic justifications and social support a given proposal gathered, the more persuasive it becomes.

7. When evaluating risks and benefits, the primary criterion should be long-term value of BCH the currency, demonstrated by purchasing power. Other criteria may be taken into account, but a good way to judge their soundness is their net benefits or risks to long term BCH value.

8. A proposal should make good-faith evaluations of its alternatives.

9. In order to minimize its risks, a proposal needs sufficient time between its publication and possible activation in order for its assertions to be widely verified and agreed upon. If activation is agreed upon, sufficient time must be allowed for wider circles of stakeholders to test and deploy the changes. The consequence of insufficient evaluation time has been evident throughout BCH's history, and should be wisely avoided.

10. It is inevitable that different parties will disagree on timelines for a proposal's evaluation and activation, which can lead to instability with no clear way to resolution. A widely known set of schedule and criteria for evaluation is expected to result in fewer controversies, and more importantly allow clearer outcomes for such controversies leaving no open-ended conflicts for each scheduled period.

11. Inability to agree on a common set of clear, verifiable criteria will inevitably lead to conflicts that has no clear outcome in a decentralized setting. Such open-ended conflicts may result in either chaos or utter centralization of decision power, neither of which are desirable.

12. There are multiple stakeholders who are interested in long term positive outcomes for BCH value. An ideal proposal should consider at least the voice of miners, BCH-focused businesses, multicoin businesses, coin holders and end-users who are often represented by businesses.

13. Stakeholders may be motivated to take part in decisionmaking processes, but care should be taken to avoid unrealistic participation assumptions. An ideal process should strive to minimize participation burden of these actors.

14. Competent developers are a scarce resource for any coin, and almost necessarily the party who are best equipped to evaluate technical benefits and limitations of any proposal, essential to their success. While proposals can originate from any party, proponents would be wise to seek involvement of competent protocol developers to maximize chances for success.

15. Each proposal will need clear proponent(s) to take ownership and drive it through evaluation. Absence of clear owners can easily lead to mixed messages, which can delay clear adoption or rejection, and lead to conflicts.

16. The community should strive to reject extra-process campaigning that appeal to emotions or soundbites, especially those without good faith risk/reward and technical evaluations. Accommodating such campaigns needlessly risks destabilization, which can lead to both short term and long term loss of value, as evident in BCH's history.

## Scheduled upgrade

Unclear schedules that leave proposals open-ended can easily lead to instability and prolonged conflicts, undermining confidence in the chain. An orderly, scheduled approach with defined milestones and cutoffs through each cycle shall be an improvement over the status quo in Bitcoin Cash. We recommend that consensus-sensitive proposals should follow the following schedule and evaluation process.

The cycle of milestones starts May 15th, 2021, with each cycle lasting a year. This cycle provides a longer, structured evaluation period for proposals. It also provides a six-month deployment period from lock-in to network-wide activation, enabling businesses to test, deploy and develop around the next upgrade with minimal disruption to their operations.

### Milestones

May 15th: Previous upgrade (if any) activation day.

Early May: Proposals for next cycle should already be under work and/or circulated, with clear personal or organizational owners.

Early June: Proposals for the next upgrade should be published by this date, complete with technical description, risk/cost/benefit analysis, evaluation of alternatives and preliminary surveys among prominent stakeholders in the ecosystem.

Early August: For proposals that were published and did not result in major controversies, functional implementations from at least one full node client should be available by this date. After this date, the implementation should then be subject to testing on a separate testnet, evaluating impact on popular applications and services. Technical reports from tests should be published and circulated among stakeholders for feedback during evaluation.

Proposals that face strong opposition from prominent businesses, stakeholders, miners or face significant, high-level technical difficulties should not be considered for November lock-in, but instead seek to alleviate concerns in time for the cycle after. Note that a given proposal must have spent reasonable effort in actively contacting its self-defined stakeholders (see below) when evaluating reception, instead of assuming consent from parties who did not respond to broadcast publications.

Early October: If the one-client implementation of said change is sufficiently evaluated, did not raise major controversies or inadequacies, and have received sufficient support, by this date there should be more than one full node team that implements the change. Specification for the change should be finalized by this date as well. Node teams should begin to produce production-ready releases for their users to upgrade early. Wider testing commences, applications are encouraged to start developing for the new features on upgrade testnet.

November 15th "lock-in": If no major bugs or emergencies were found during wider testing, all full node implementations are expected to deliver a compatible production release by this date, or cease to be recommended for BCH until they do. Deployment should be widely and aggressively pushed throughout the ecosystem to help migration and diagnose possible edge cases.

Note that proposals that made it to November 15th should be considered locked in as a rule for implementation across the ecosystem.

May 15th of the following year: New upgrade activation date.

### Expectations on no-op "upgrade days"

A regularly scheduled upgrade, and enforcing expiration mechanisms, are merely convenient Schelling points to have orderly consensus changes.
They must not be seen as mandates for having _at least some_ consensus changes, and indeed simple extensions of the expiration mechanism without consensus change, leaving upgrades to the next cycle, should be an acceptable outcome for any given cycle if no proposal can get wide agreement and robust testing by the aforementioned milestones.

### Consensus-building

As each node team is ultimately responsible for writing their own software, decisions about each proposal's inclusion must necessarily be agreed upon by each team themselves. It is important to note that this process does not, and in fact cannot, coerce any software team to implement or not implement any given change. Its power to persuade disagreeing node teams is largely dependent on the community and ecosystem's willingness to follow it, and use only software that follows its outcome.

The landscape of Bitcoin Cash stakeholders is diverse, and can change significantly over time. Each proposal should make good-faith attempts to self-define relevant, persuasive groups to be consulted in the "Stakeholder groups", "Known support/opposition" and "Comments" sections below. An important feature of this process is that it must first define a reasonably wide set of relevant stakeholders, then seek overwhelming support from this set to be persuasive.

### Dispute resolution

We expect most serious, good-faith proposals to have clear outcomes, whether acceptance or rejection, that are unambiguous to both stakeholders and node developers. However, it is inevitable that from time to time popular node teams and stakeholders will disagree with proposal owners or detractors about a given proposal's outcome.

The process below outlines an objective, costly way to resolve such disputes, which should only be invoked very rarely. Proposal owners following and succeeding in this process should gain considerable ecosystem-wide persuasive power, as observed in the events preceding DAA upgrade of 2020.

At any time between May 15th and October 15th, a party seeking dispute resolution against a major node team should complete the following:

1. Gain a sustained supporting signaling from over 75% of the BCH hashpower for at least six weeks, via coinbase transactions.

2. Gain signed, verifiable support from at least 50,000 BCH in contemporaneous UTXOs, with a combined coindays of at least 3,000,000 BCH-days. In order to increase confidence, each stake should include or be accompanied by a similarly verifiable statement of identity, pseudonymous or otherwise. If an opposition effort is present, each of these metrics must exceed its opposition by at least a 3:1 margin.

3. Gain public support from at least 1 out of the top 5 custodial exchanges by total BCH volume. If public opposition is present, the supporting number of top exchanges must exceed opposition by at least a 2:1 margin.

It should be noted that this dispute resolution process still cannot, in the strictest sense, coerce any given node team into implementing any change. The unavoidable high cost and publicity for its completion, however, shall greatly increase the proposal's persuasive power, so that the node in dispute can either be convinced or abandoned. Its failure should have similar effects in the opposite direction, in that the cost of destabilization can be clearly demonstrated.

While futures markets have been used in the past as a persuasive indicator, it should be noted that futures markets are even more costly to establish than the above, and are typically only created when irreconcilable differences are leading to likely consensus forks. If such a market becomes reality, persuasion of existing node teams or proposal proponents/detractors is likely moot.

### End of regular schedules

It is quite possible that we can eventually converge on a more robust decisionmaking process than scheduled upgrades and the rare and costly dispute-resolution.
When such a process emerges, it should be subject to the same schedule, milestones and rigor as other proposals, and upon activation, the processes outlines in this document should cease to be recommended in favor of the new process.

## Motivation and Benefits

This section should describe the expected benefits to BCH stakeholders from the proposal's acceptance. It is recommended that these benefits be tied to long-term BCH value.

In the context of this document, a predictable, transparent process that can be objectively evaluated can greatly reduce uncertainty and conflict among parties, as persuasive, public evaluations are attached to each outcome. It has the following benefits:

1. Proponents or detractors of proposals can be better convinced in a public process, and less likely to resolve via extreme forking or hashwar measures that result in value loss.

2. Businesses, miners and holders can follow and participate in the public CHIP process and gain confidence about acceptance or rejection within each cycle, therefore increasing their investment confidence in BCH both as a platform and a currency.

3. Developers can follow and participate in an orderly CHIP process, and contribute to reasonable acceptance or rejection without engaging in draining, diffuse public campaigns with no clear endpoint.

## Technical description

Unlike subsequent CHIPs, this document is unique in that its acceptance or rejection results in purely procedural change, and has only indirect software consequences. Its Technical Description, Specifications, Current Implementations and Test Cases sections are, therefore, only content recommendations for subsequent CHIPs.

This section should contain a technical definition of the proposal in more detail beyond "Summary", including but not limited to a high-level workflow description, relevant technologies used, and the scope of expected software alterations.

## Specification

This section should contain detailed specifications of the proposal, including but not limited to protocol formats, parameter values and expected workflows. It can be updated as the CHIP makes its way through evaluation and testing, but the owner is expected to make a good-faith attempt at first publication.

## Current Implementations

This section should contain code examples and proof-of-concepts of the CHIP's proposed change, or links to them. It can be updated as the CHIP makes its way through evaluation and testing.

## Test Cases

This section should contain test cases that can be used to evaluate new implementations against existing ones. It can be updated as the CHIP makes its way through evaluation and testing.

## Security Considerations

This section should contain foreseeable security implications to the network. Common technical considerations include consensus risk, denial-of-service risk, and ongoing maintenance burden from additional complexity that can lead to rising likelihoods of either.

## Evaluation of Alternatives

This section should describe significant alternatives to the proposal, and a brief comparison of each of their benefits/costs.

In the context of the processes outlined in this document, the following alternatives currently exist:

### No schedule / No process

Description: Remove the expiration mechanism altogether from all clients, and expected upgrade schedules associated with it. Upgrade "when ready".

Benefits:

1. Offers the most flexibility under idealized conditions. Proposals would not be subject to undue "make it or wait another year" deadlines, and can proceed at an optimal pace in research, implementation and consensus-building.
2. Default mode of operation for some of the biggest coins like BTC and ETH.

Costs:

1. We do not have a fully convincing way to decide when to "seal the deal" on any upgrade yet. While many parties in BCH are committed to developing a long-term solution to decisionmaking, no mature and robust process exists right now. Proposals of miner voting, stakevoting and other decisionmaking schemes of various sophistication abound, each with glaring pitfalls and tradeoffs that need time to be worked out. None are likely to be available in a sufficiently convincing manner for May 2021, before which a decision will need to be made.
2. Without a robust process, going no-schedule in a decentralized landscape can easily stall beneficial upgrades indefinitely where support and opposition are difficult to measure, resulting in similar or worse woes as a long cycle. It will also be impossible to set expectations on progress for any given proposal, making conflict over how "ready" a proposal is more controversial. Having a defined schedule to evaluate proposals makes worst-case long-term stagnation less likely while better processes are being researched and refined.
3. Prolonged uncertainty and unclear outcomes may eventually lead to de facto dictatorships, where one most influential developer or stakeholder make unilateral decisions. Such outcomes are simply a natural consequence of businesses and users desiring confidence. Unfortunately, dictatorships are easily abused, as seen in the 2020 IFP controversy, and should be pre-empted where possible by alternative, orderly arrangements.

### Shorter schedule, such as a six-month cycle

Benefits:

1. Status quo before November 2020, as enforced by Bitcoin ABC releases.
2. Under ideal conditions, enables rapid iteration of beneficial consensus alterations, enabling the ruleset to cater to market needs and changes.

Costs:
1. In theory, longer periods of research, analysis and presentation beyond the six-month cycle should have been able to converge stakeholders towards one set of consensus upgrades. In practice, the community only pays attention to the next set of changes after doing the work from last one, and not nearly enough time has been spent on consensus building. This presented opportunities for influential figures to attempt ramming through their favorite changes without considering costs, both economic and social, to the wider ecosystem.
2. Past evidence heavily suggests that six-month cycles were not enough to research and resolve issues in each upgrade, resulting in poor outcomes even where the upgrade was uncontroversial, as seen in the May 2019 upgrade. Controversial upgrades were further exacerbated by the urgency and resulted in significant loss of value, as seen in the Nov 2018 network split.

### Longer schedule, such as a two-year cycle

Benefits:

1. Longer cycles allow even more time for research and consensus-building to play out.
2. Long cycles can minimize time spent in periods of uncertainty, inspiring confidence for businesses and investors who seek stable foundations for their enterprise.

Costs:

1. Technology in cryptocurrency moves rather quickly. While its larger sibling BTC can spend multiple years debating even less consequential upgrades like Taproot, BCH does not have the luxury of expecting existing network effect to carry it through very long periods of time. If necessary usecase and scalability improvements are delayed for too long, the chain can lose its competitive advantages and become irrelevant.
2. Over a longer time, the infrastructure, processes and social will necessary to even evaluate new proposals will likely atrophy.
3. Given a long enough cycle, impatient parties may paradoxically gain enough support to cause damaging community and network splits.

### Direct miner or stakeholder voting

Description: Instead of the persuasion process in this CHIP that only rarely involves miner and stakeholder "vote" to resolve disputes, invoke such voting for every proposal.

Benefits:

1. As miners ultimately enforce network rules and economic actors give them value, direct votes offer the strongest possible support or rejection for any given proposal.
2. The process can be strictly defined on a technical level leaving little room for ambiguity, an example of which can be seen in [BIP 135](https://github.com/bitcoin/bips/blob/master/bip-0135.mediawiki).

Costs:

1. A representative vote among most pools and the entire ecosystem can be very difficult to implement, historically it is difficult to get high participation for all but the most urgent of crisis. Such difficulty is likely to lead to ossification if a voting-only process is adopted.
2. Repeated invocation of the voting process places a high burden on all actors, a cost that can lead to the process being abandoned over time.

## Implementation costs and risks

This section should list immediate costs and risks to the ecosystem if the proposal is adopted. This may include developer labor, adaptation of downstream software, risk of dissatisfaction leading to a major split, and other relevant concerns. Similar to "benefits", it is recommended that costs and risks be described in the context of their consequences to long term BCH value.

In the context of this document, there are two major concerns:

1. While not strictly coercive, node developers can feel compelled to respond to all decently-written proposals as well as test them. Independent of their ultimate acceptance or rejection, this imposes an overhead that can compete for developer resources with non-consensus maintenance, performance and security work. Our opinion is that this risk is an acceptable tradeoff for the potential structure and ease of mind offered by the process.

2. Parties who disagree with the process may immediately initiate damaging, costly public campaigns against it, leading to value loss. Such disagreements, however, are more likely to reach a definitive outcome under a structured, well-informed process, so their damage can be limited.

## Ongoing costs and risks

This section should list ongoing costs and risks to the ecosystem if the proposal is adopted, beyond immediate activation-time concerns. This may include node operator costs, software maintenance burden, loss of utility, loss of reputation and so on.

In the context of CHIPs as outlined in this document, there are at least two major concerns:

1. Adoption of a clear schedule can turn out to be too inflexible.

2. The proposal and consensus-seeking process may introduce significant overhead to proponents of simpler changes with marginal benefits. Changes with small benefits may be prevented from being proposed in the first place if their proponents cannot justify investing in overcoming said overhead.

## List of Major Stakeholders

This section should list relevant stakeholder groups who need to be consulted. The proposal owner should include a wide, persuasive selection from major actors in each of the groups in "Statements" below.

This document is unique in that it is not a specific change to any Bitcoin Cash protocol, so its stakeholders are future upgrade proponents and skeptics, who can express their support for the process by their participation. For subsequent CHIPs that propose specific changes to the protocol, they are expected to include as wide a selection as practical to be persuasive. Skeptics should not hesitate to publicly point out glaring omissions.

## Statements

This section collects comments from the above group of stakeholders, whether they are supportive, skeptical or otherwise providing relevant input. CHIP owners should be responsible for populating this section as far as possible to be persuasive to node developers and other stakeholders alike.

### Full node developers

### Major businesses

### Miners and pools

### Application developers

### Service operators

## License

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
